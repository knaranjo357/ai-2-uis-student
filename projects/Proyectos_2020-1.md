---
# Proyectos 2020-1. Inteligencia Artificial II: Deep Learning

## Prof: Fabio Martínez, Ph.D

---

# Lista de Proyectos
1. [Cuantificación y predicción de mecanismos de desgaste en imágenes de superficies degradadas](#proy1)
2. [DETECCIÓN DE NEUMONÍA EN IMÁGENES DE RAYOS-X UTILIZANDO REDES NEURONALES](#proy2)
3. [](#proy3)
4. [Automatic aorta segmentation](#proy4)
5. [NASMCC: Nuclear atypia scoring and Mytotic count detector](#proy5)
6. [Clasificación de vídeos de temblor de manos asociado a pacientes con Parkinson](#proy6)
7. [Speech to command ](#proy7)
8. [Clasificación de emociones usando NLP](#proy8)
9. [Red para la detección de malaria](#proy9)
10. [SEGMENTACIÓN Y CLASIFICACIÓN DE ESTADÍOS DE CÁNCER DE PRÓSTATA SEGÚN LA ESCALA DE GLEASON](#proy10)

---

## Cuantificación y predicción de mecanismos de desgaste en imágenes de superficies degradadas <a name="proy1"></a>

**Autores: Brayan Camilo Valenzuela Rincón**

<img src="https://gitlab.com/Brayan01/analisis-de-desgaste-en-imagenes-de-superficies-degradadas/-/raw/master/Banner-AI2.jpg" style="width:700px;">

**Objetivo: Predecir mecanismos locales de desgaste en imágenes de superficies degradadas.**

- Dataset: Imágenes de microscopía de barrido electrónico de placas revestimiento, suministradas por el grupo de investigación en corrosión. 
- Modelo: Características de aprendizaje profundo  y vectores embebidos de espacios latentes, análisis de componentes principales.


[(code)](https://gitlab.com/Brayan01/analisis-de-desgaste-en-imagenes-de-superficies-degradadas) [(video)](https://www.youtube.com/watch?v=jcftlWSzeOE&feature=youtu.be) [(+info)](https://gitlab.com/Brayan01/analisis-de-desgaste-en-imagenes-de-superficies-degradadas/-/blob/master/Systems_Fest__2__compressed.pdf)

---

## DETECCIÓN DE NEUMONÍA EN IMÁGENES DE RAYOS-X UTILIZANDO REDES NEURONALES <a name="proy2"></a>

**Autores: Natalia Gómez Albiadez. **

<img src="https://raw.githubusercontent.com/nataliaalbiadez/Detecci-n-de-Neumonia-en-Imagenes-de-Rayos-X-Utilizando-Redes-Neuronales/master/Banner.PNG" style="width:700px;">

**Objetivo: Desarrollar un algoritmo de clasificación de imágenes de rayos-X con el fin de apoyar al diagnóstico de neumonía utilizando modelos de aprendizaje profundo y transferencia de aprendizaje.  **

- Dataset: onjunto de datos de imágenes de rayos-X adquiridas en el Centro Médico de Niños y Mujeres de Guangzhou. [Disponible](https://www.kaggle.com/paultimothymooney/detecting-pneumonia-in-x-ray-images)
- Modelo: Transfer learning

[(code)](https://github.com/nataliaalbiadez/Detecci-n-de-Neumonia-en-Imagenes-de-Rayos-X-Utilizando-Redes-Neuronales) [(video)](https://www.youtube.com/watch?v=dt3vn9E6JB4&t=1s) [(+info)](https://github.com/nataliaalbiadez/Detecci-n-de-Neumonia-en-Imagenes-de-Rayos-X-Utilizando-Redes-Neuronales/blob/master/Presentacion.pdf)

---
## title <a name="proy3"></a>

**Autores: Edgar Andrés Montenegro**

<img src="https://raw.githubusercontent.com/EdgarAndresMontenegro/Zebrafish_Behaver_Net/master/BannerFinalZebrafish.png" style="width:500px;">

**Objetivo: **

- Dataset: 
- Modelo: 

[(code)](https://github.com/EdgarAndresMontenegro/Zebrafish_Behaver_Net) [(video)](https://github.com/EdgarAndresMontenegro/Zebrafish_Behaver_Net/blob/master/CuantificacionDePatrones_PezCebra_DESCRIPCION.mp4) [(+info)](https://github.com/EdgarAndresMontenegro/Zebrafish_Behaver_Net/blob/master/ZebrafishPresentation_IA.pdf)

---

## title <a name="proy4"></a>

**Autores: Eliana Arenas, Liceth Rozo **

<img src="https://raw.githubusercontent.com/LicethYaneth/Aorta-segmentation/master/banner.png" style="width:400px;">

**Objetivo: Realizar una segmentación automática de aorta abdominal, usando técnicas de Deep learning.**

- Dataset: .
- Modelo: CNN

[(code)](https://github.com/LicethYaneth/Aorta-segmentation) [(video)](https://youtu.be/YHSMqp8ORF8 ) [(+info)](https://github.com/LicethYaneth/Aorta-segmentation/blob/master/ProyectoI.A_SegmentacionAorta.pdf)

--- 

## NASMCC: Nuclear atypia scoring and Mytotic count detector <a name="proy5"></a>

**Autores:  María Fernanda Navas Burgos y Juan Pablo Moreno Ríos**

<img src="https://raw.githubusercontent.com/Momo-owo/NASMCC/master/banner.png" style="width:700px;">

**Objetivo: Desarrollar arquitecturas dentro del marco del aprendizaje profundo para detectar anomalías relacionadas con el cáncer de mama en histopatologías**

- Dataset: 4267 Imágenes tomadas del dataset MITOS-ATYPIA-14 que se puede encontrar en https://mitos-atypia-14.grand-challenge.org/

- Modelo: 

[(code)](https://github.com/Momo-owo/NASMCC) [(video)](https://www.youtube.com/watch?v=8wIMpkNAhUQ) [(+info)](https://github.com/Momo-owo/NASMCC)

---

## Clasificación de vídeos de temblor de manos asociado a pacientes con Parkinson <a name="proy6"></a>

**Autores: Jessica Pedraza **

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-2-uis-student/-/raw/master/resources/jessica.png" width="400">

**Objetivo: **

- Dataset: Conjunto de secuencias de video capturadas por el grupo de Investigación BIVL2ab
- Modelo: CNN


[(code)](https://github.com/jessicapedraza29/IA2) [(video)]( https://youtu.be/z0NAfQAU7oo) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-2-uis-student/-/raw/master/resources/jessica.pdf)

---

## Speech to command <a name="proy7"></a>

**Autores:Eduard Alfonso Caballero, María Camila Aparicio, Iván Rodrigo Castillo **

<img src="https://raw.githubusercontent.com/ivanrcas/speechtocommand/master/proyecto/banner_speech_to_command_IA2.jpg" width="700">

**Objetivo: Reconocer comandos de voz simples en inglés mediante una interfaz que registra la voz y ejecuta acciones de acuerdo a comandos detectados en audios grabados, facilitando la interacción y accesibilidad con dispositivos inteligentes.**

- Dataset: Cerca de 65k audios (formato WAV) de comandos grabados en inglés por diferentes tonos de voz, cada uno con duración de un segundo, dividido en 30 clases o tipos de comandos.
- Modelo: DNN, RNN, LSTM y GRU


[(code)](https://github.com/ivanrcas/speechtocommand/blob/master/proyecto/notebook_speech_to_command_IA2.ipynb) [(video)]( https://www.youtube.com/watch?v=eDRIdYVA9E4) [(+info)](https://github.com/ivanrcas/speechtocommand/blob/master/proyecto/slides_speech_to_command_IA2.pdf)

---
## Clasificación de emociones usando NLP  <a name="proy8"></a>

**Autores: Jorge Andres Mogotocoro Fajardo, Elkin Darío Fernández Celis**

<img src="https://camo.githubusercontent.com/b2ef0ef06d5acb8a4b1600e697a34052e5ad385b/68747470733a2f2f7265732e636c6f7564696e6172792e636f6d2f6a616d6630352f696d6167652f75706c6f61642f76313539393334313634322f72616e646f6d2d75706c6f6164732f6e74666c62703435356f6d32796876736166626d2e6a7067" width="700">

**Objetivo: Clasificar oraciones etiquetadas con un sentimiento usando NLP.**

- Dataset: emotions-dataset-for-nlp , WASSA-2017 Shared Task on Emotion Intensity, nlp-text-emotion, simbig2016-facebook-reactions, facebook Reactions 
- Modelo: LSTM y GRU


[(code)](https://github.com/Jamf05/20201-ai2-class-project) [(video)](https://www.youtube.com/watch?v=pgi9X9bHW7U&feature=youtu.be) [(+info)](https://github.com/Jamf05)

---

## Red para la detección de malaria <a name="proy9"></a>

**Autores: Daniel Perez, Victor  Mantilla, Jhoan Manuel Diaz**

<img src="https://raw.githubusercontent.com/danielperezaltmar/Inteligencia-artificial-2/master/banner.png" width="700">

**Objetivo: clasificar entre imágenes de células con el parásito de la malaria y sin este**

- Dataset: https://www.kaggle.com/iarunava/cell-images-for-detecting-malaria.
- Modelo: ResNet50, VGG16 y VGG19.


[(code)](https://github.com/danielperezaltmar/Inteligencia-artificial-2) [(video)](https://youtu.be/lTU_NH7qDq4) [(+info)](https://github.com/danielperezaltmar/Inteligencia-artificial-2)

---

## SEGMENTACIÓN Y CLASIFICACIÓN DE ESTADÍOS DE CÁNCER DE PRÓSTATA SEGÚN LA ESCALA DE GLEASON <a name="proy10"></a>

**Autores:  Andrés Felipe Gómez Ortiz, Maria Fernanda Vera Negrón**

    
<img src="https://gitlab.com/mariavera/gleason-segmentation/-/raw/master/img/bannerGleason2.png" width="700">

**Objetivo: Desarrollar e implementar una estrategia para la segmentacion y clasificación de estadíos relacionados a la agresividad del cáncer de próstata según la escala de Gleason.**

- Dataset:  Se utilizó un Dataset de Harvard Dataverse con 886 imágenes de 3100 x 3100 de muestras de tejido prostático teñidas con H&E a una resolución de 40x (0,23 micras por píxel) y agrupadas en cinco microarrays de tejido (TMA), etiquetadas por expertos patólogos siguiendo la escala de Gleason. https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/OCYCMP 
- Modelo: Mask R-CNN


[(code)]( https://gitlab.com/mariavera/gleason-segmentation/-/tree/master ) [(video)](https://www.youtube.com/watch?v=nG9PKhqly_I ) [(+info)](https://gitlab.com/mariavera/gleason-segmentation/-/tree/master/presentacion)

