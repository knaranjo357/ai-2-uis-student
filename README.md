# Inteligencia Artificial II 2020-2

## Bienvenidos!

<img src="/imgs/Banner_IA2_1.png" style="width:400px;">



## Colaboratory (Google)

Vamos a utilizar la plataforma de google para editar, compartir y correr notebooks: [**Colaboratory**](https://colab.research.google.com/notebooks/welcome.ipynb) 

- Necesitas una cuenta de gmail y luego entras a drive
- Colaboratory es un entorno de notebook de Jupyter gratuito que no requiere configuración y se ejecuta completamente en la nube.
    - Usaremos parte de la infraestructura de computo de google... gratis! (máximo procesos de 8 horas)
- Con Colaboratory, puedes escribir y ejecutar código, guardar y compartir análisis, y acceder a recursos informáticos potentes, todo gratis en tu navegador.
- También puedes usar los recursos de computador Local. 


## Calificación

20% Talleres primer corte (Problemsets)
20% Talleres segundo corte (Problemsets)
20% Talleres tercer corte (Problemsets)
40% Proyecto funcional IA 

## Talleres (Problemsets)

Los talleres pretenden ser una herramienta practica para afianzar los conocimientos desarrollados durante las clases. En general se presentan como un conjunto de ejercicios que serán desarrollados **individualmente** por los estudiantes. Cada taller esta escrito como un notebook para la validación automática. Se pueden hacer tantos intentos como se quieran y unicamente la última respuesta será tomada en cuenta. Cada uno de los talleres ser desarrollará en casa, dentro de las fechas establecidas en el cronograma. 



## Proyecto funcional IA2

- **Funcionamiento del proyecto**: El proyecto se debe realizar como un notebook y debe ser 100% funcional.

- **Prototipo (PRE-SUS PROJ)**: En este item se considera como esta estructurado el proyecto y se espera una nivel razonable de funcionalidad.

- **Presentación**:
Imagen relacionada con la siguiente información: título del proyecto e información de los estudiantes<br>
Video corto (ENTREGAR EL ARCHIVO DE VIDEO y también alojarlo en youtube)<br>


- **Sustentación**: Se realizarán preguntas cortas a los estudiantes unicamente relacionadas con el proyecto. 
 
Todos los items tienen el mismo porcentaje de evaluación. 

**UNICAMENTE SE TENDRAN EN CUENTA LOS PROYECTOS QUE SE HAYAN POSTULADO AL FINALIZAR EL PRIMER CORTE**-->


## Calendario y plazos

                        SESSION 1            SESSION 2              SESSION SATURDAY

     W01 Oct20-Oct21    Intro IA2==DL            Fundamental-ML
     W02 Oct27-Oct28    Fundamental-ML-Reg       Fundamental-ML-Clas
     W03 Nov03-Nov04    DNN-Reg_and_Backprop     DNN-Class_and_Backprop
     W04 Nov10-Nov11    DNN-Regression           DNN-tricks
     W05 Nov17-Nov18    CNN Fundamentals         CNN-tricks
     W06 Nov24-Nov25    CNN_architectures        CNN_architectures
     W07 Dic01-Dic02    CNN_TL                   CNN_TL-aug-feat     Dic 4-Taller 1-1                  
     W08 Dic08-Dic09    -----                    TALLER-PARCIAL      Dic 19-Taller 1-2
     W09 Dic15-Dic16    RNN:fundamentals         RNN:fundamentals
     
     -----------------  VACACIONES -----------------
     
     W10 Ene19-Ene20    Welcome Back             RNN:fundamentals
     W11 Ene26-Ene27    RNN-TemporalSeries       RNN-TemporalSeries
     W12 Feb02-Feb03    RNN-in depth             RNN-textGeneration
     W13 Feb09-Feb10    LSTM-textGeneration      TALLER-PARCIAL
     W14 Feb16-Feb17    TALLER-PARCIAL           Autoencoders-CNN 
     W15 Feb23-Feb24    Autoencoders-CNN         UNET
     W16 Mar02-Mar03    GANS                     TALLER-PARCIAL
     W17 Mar09-Mar10    SUS PROJ                 SUS PROJ


    Dic 11 -           -> Registro primera nota
    Dic 13 -           -> Último día cancelación materias
    Mar 03             -> Finalización clase
    Dic 21 - Ene 13    -> Vacaciones
    Mar 04 - Mar 11    -> Evaluaciones finales
    Mar 17 -           -> Registro calificaciones finales
    

[Calendario academico](https://www.uis.edu.co/webUIS/es/academia/calendariosAcademicos/2020/acuerdoAcad294_2020.pdf)

**CUALQUIER ENTREGA FUERA DE PLAZO SERÁ PENALIZADA CON UN 50%**

**LOS PROBLEMSETS ESTAN SUJETOS A CAMBIOS QUE SERÁN DEBIDAMENTE INFORMADOS**

**DEADLINE DE LOS PROBLEMSETS SERÁ EL DIA DE CADA PARCIAL**

